# Stanford Course CS231n - Convolutional Neural Networks for Visual Recognition (Spring 2018)


Lecture notes, assignments and other materials can be downloaded from the
[course webpage](http://cs231n.stanford.edu).
 - Lecture videos: [YouTube](https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv)
 - Syllabus and notes: [Stanford CS231n](http://cs231n.stanford.edu/syllabus.html)


## Homework
 - [X] Assignment 1: [http://cs231n.github.io/assignments2018/assignment1/](http://cs231n.github.io/assignments2018/assignment1/)
 - [X] Assignment 2: [http://cs231n.github.io/assignments2018/assignment2/](http://cs231n.github.io/assignments2018/assignment2/)
 - [X] Assignment 3: [http://cs231n.github.io/assignments2018/assignment3/](http://cs231n.github.io/assignments2018/assignment3/)


## Prerequisites

- Proficiency in Python, high-level familiarity in C/C++
  All class assignments will be in Python (and use numpy) (we provide a tutorial here for those who aren't as familiar
  with Python), but some of the deep learning libraries we may look at later in the class are written in C++.
- College Calculus, Linear Algebra
  You should be comfortable taking derivatives and understanding matrix vector operations and notation.
- Basic Probability and Statistics - [CS 109](http://cs109.stanford.edu)
  You should know basics of probabilities, gaussian distributions, mean, standard deviation, etc.
- Equivalent knowledge of [CS 229](http://cs229.stanford.edu)
  We will be formulating cost functions, taking derivatives and performing optimization with gradient descent.


## Course Description

Computer Vision has become ubiquitous in our society, with applications in search, image understanding, apps, mapping, medicine, drones, and self-driving cars. Core to many of these applications are visual recognition tasks such as image classification, localization and detection. Recent developments in neural network (aka “deep learning”) approaches have greatly advanced the performance of these state-of-the-art visual recognition systems. This course is a deep dive into details of the deep learning architectures with a focus on learning end-to-end models for these tasks, particularly image classification. 

During the 10-week course, students will learn to implement, train and debug their own neural networks and gain a detailed understanding of cutting-edge research in computer vision. The final assignment will involve training a multi-million parameter convolutional neural network and applying it on the largest image classification dataset (ImageNet). We will focus on teaching how to set up the problem of image recognition, the learning algorithms (e.g. backpropagation), practical engineering tricks for training and fine-tuning the networks and guide the students through hands-on assignments and a final course project.
 
Much of the background and materials
of this course will be drawn from the [ImageNet Challenge](http://image-net.org/challenges/LSVRC/2014/index).
